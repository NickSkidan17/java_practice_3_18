package impl;

import api.Car;
import api.TaxiPool;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Comparator;

@AllArgsConstructor
public class TaxiPoolImpl implements TaxiPool {
    @Getter
    @Setter
    private ArrayList<Car> cars;

    @Override
    public void sortTaxiPoolByFuelConsumption() {
        cars.stream().sorted(Comparator.comparing(Car::getFuelConsumption)).forEach(car -> System.out.println(car));
    }

    @Override
    public int getTaxiPoolPrice() {
        return cars.stream().reduce(0, (sum, car) -> sum += car.getPrice(), (sum1, sum2) -> sum1 + sum2);
    }

    @Override
    public void selectCarsInSpeedInterval(int minSpeed, int maxSpeed) {
        cars.stream().filter(car -> car.getTopSpeed() >= minSpeed && car.getTopSpeed() <= maxSpeed).forEach(car -> System.out.println(car));
    }
}

