package entity;

import api.Car;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString(of = {"mark", "model", "fuelConsumption", "price", "topSpeed", "fuelType", "transmissionType", "passengerSeatsNumber"})
public class PassengerVehicle implements Car {
    private String mark;
    private String model;
    private int topSpeed;
    private Integer fuelConsumption;
    private int price;
    private String fuelType;
    private String transmissionType;
    private int passengerSeatsNumber;
}