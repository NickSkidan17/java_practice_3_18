package entity;

import api.Car;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString(of = {"mark", "model", "fuelConsumption", "price", "topSpeed", "driveType", "trunkVolume"})
public class OffroadVehicle implements Car {
    private String mark;
    private String model;
    private int topSpeed;
    private Integer fuelConsumption;
    private int price;
    private String driveType;
    private double trunkVolume;
}
