package api;

public interface Car {

    int getTopSpeed();

    Integer getFuelConsumption();

    int getPrice();

}