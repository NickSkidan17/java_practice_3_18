package api;

public interface TaxiPool {

    int getTaxiPoolPrice();

    void sortTaxiPoolByFuelConsumption();

    void selectCarsInSpeedInterval(int minSpeed, int maxSpeed);

}